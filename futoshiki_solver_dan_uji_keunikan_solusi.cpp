//program tugas akhir kuliah

#include <iostream>
#define N 4
using namespace std;
int data[N][N] = {{0, 0, 0, 0},{0, 0, 0, 0},{0, 0, 0, 0},{2, 0, 0, 0}};
string tanda_baris[N][N-1] = { // TANDA PERTAKSAMAAN PADA BARIS
{">"," "," "}, // tanda > untuk lebih dari
{" "," "," "}, // tanda < untuk kurang dari
{" "," "," "}, // beri spasi jika kosong
{">"," "," "},
};
string tanda_kolom[N-1][N] = { // TANDA PERTAKSAMAAN PADA KOLOM
{" "," ","^"," "}, // tanda v untuk lebih dari >
{" "," ","v"," "}, // tanda ^ untuk kurang dari <
{" "," "," ","v"}, // beri spasi jika kosong
};
int jumlah_solusi = 0;
bool cekTanda(int row, int col,int num) {
if (col != 0) { // CEK KIRI
 if (tanda_baris[row][col-1] == "<" && data[row][col-1] > num)
 return false;
 else if (tanda_baris[row][col-1] == ">" && data[row][col-1] < num)
 return false;
}
if (col != N-1) { // CEK KANAN
 if (tanda_baris[row][col] == "<" && num > data[row][col+1])
 if (data[row][col+1] != 0)
 return false;
 else if (tanda_baris[row][col] == ">" && num < data[row][col+1])
 if (data[row][col+1] != 0)
 return false;
}
if (row != 0) { // CEK ATAS
 if (tanda_kolom[row-1][col] == "^" && data[row-1][col] > num)
 return false;
 else if (tanda_kolom[row-1][col] == "v" && data[row-1][col] < num)
 return false;
}
if (row != N-1) { // CEK BAWAH
 if (tanda_kolom[row][col] == "^" && num > data[row+1][col])
 if (data[row+1][col] != 0)
 return false;
 else if (tanda_kolom[row][col] == "v" && num < data[row+1][col])
 if (data[row+1][col] != 0)
 return false;
}
return true; // JIKA TIDAK MELANGGAR TANDA
}
bool cekKolom(int col, int num) {
for (int row = 0; row < N; row++)
 if (data[row][col] == num) // MENGECEK ANGKA SAMA ATAU TIDAK
 return false; // JIKA SAMA, KENDALA TIDAK TERPENUHI
return true; // JIKA TIDAK DITEMUKAN SAMA, KENDALA TERPENUHI
}
bool cekBaris(int row, int num) {
for (int col = 0; col < N; col++)
32
 if (data[row][col] == num) // MENECEK ANGKA SAMA ATAU TIDAK
 return false; // JIKA SAMA, KENDALA TIDAK TERPENUHI
return true; // JIKA TIDAK DITEMUKAN SAMA, KENDALA TERPENUHI
}
void cetakSolusi() {
for (int row = 0; row < N; row++) {
 for (int col = 0; col < N; col++) {
 cout<<data[row][col]; // MENCETAK ANGKA
 if (col != N-1) // CETAK TANDA BARIS
 cout<<" "<<tanda_baris[row][col]<<" ";
 }
 cout<<endl;
 for (int col2 = 0 ; col2 < N; col2++)
 if (row != N-1) // CETAK TANDA KOLOM
 cout<<tanda_kolom[row][col2]<<" ";
 cout<<endl;
}
}
bool cariSelKosong(int &row, int &col) {
for (row = 0; row < N; row++)
 for (col = 0; col < N; col++)
 if (data[row][col] == 0) // CEK DATA APAKAH KOSONG
 return true;
return false;
}
bool cekValidasi(int row, int col, int num) {
return cekBaris(row,num) && cekKolom(col,num) && cekTanda(row,col,num);
}
bool cariSolusi() {
int row, col;
if (!cariSelKosong(row, col)) { // MENCARI ROW, COL DARI SEL KOSONG
 jumlah_solusi++;
 if (jumlah_solusi == 1)
 cetakSolusi(); // CETAK SOLUSI PERTAMA
 return false; // LANJUT LAGI CARI SOLUSI BERIKUTNYA
}
for (int num = 1; num <= N; num++) // AMBIL SEMBARANG ANGKA
 if (cekValidasi(row, col, num)) { // CEK VALIDASI
 data[row][col] = num; // INPUT PADA SEL JIKA VALID
 if (!cariSolusi()) // REKURSIF
 data[row][col] = 0; // HAPUS ANGKA YANG DIINPUT
 }
return false; // KEMBALI KE REKURSI SEBELUM
}
int main() { // PROGRAM UTAMA
if (!cariSolusi()) { // JALANKAN FUNGSI PENCARI SOLUSI
 if (jumlah_solusi == 0)
 cout << "Solusi tidak ditemukan"; // CETAK INI JIKA TIDAK ADA SOLUSI
 else if (jumlah_solusi == 1)
 cout << "Solusi unik"; // CETAK INI JIKA SOLUSI HANYA ADA 1
 else if (jumlah_solusi > 1)
 cout<<endl<<"Solusi tidak unik"; // CETAK INI JIKA SOLUSI BANYAK
}
cout<<endl<<"Jumlah solusi ditemukan : "<<jumlah_solusi;
}
